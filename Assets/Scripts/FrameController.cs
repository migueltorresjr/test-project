using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameController : MonoBehaviour
{
    public Text indicatorText;
    public void ShowHideFrame()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);

        indicatorText.text = gameObject.activeInHierarchy ? "Esconder Logo" : "Mostrar Logo";
    }
}
