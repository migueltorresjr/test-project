using UnityEngine;

public class FreezeInBackground : MonoBehaviour
{
    private int _frameCounter = 0;

    private void Update()
    {
        _frameCounter++;
        if (_frameCounter % 10 == 0)
        {
            ScreenInBackground();
        }
    }

    private static void ScreenInBackground()
    {
        Application.runInBackground = true;
    }
}
